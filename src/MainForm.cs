﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Mommosoft.ExpertSystem;
using TelevisorChooser.Models;
using TelevisorChooser.Parsers;

namespace TelevisorChooser {
    public partial class MainForm : Form {

        readonly string[] _preferredResolutionChoices = { "Obojętnie", "HD", "Full HD", "4K UHD" };
        readonly string[] _preferred3DChoices = { "Obojętnie", "Tak", "Nie" };
        readonly string[] _preferredCurvedChoices = { "Obojętnie", "Tak", "Nie" };
        readonly string[] _preferredHdrChoices = { "Obojętnie", "Tak", "Nie" };

        readonly string[] _wiFiChoices = { "Obojętnie", "Tak", "Nie" };
        readonly string[] _smartTvChoices = { "Obojętnie", "Tak", "Nie" };
        readonly string[] _hdmiChoices = { "Obojętnie", "1", "2", "3", "4" };
        readonly string[] _usbChoices = { "Obojętnie", "1", "2", "3" };
        readonly string[] _sizeChoices = { "Obojętnie", "22 - 32", "33 - 40", "41 - 65" };

        readonly string[] _priceChoices = { "Mogę dopłacić", "Wolę oszczędzać" };

        private readonly Mommosoft.ExpertSystem.Environment _enviroment;
        private readonly TvParser _tvParser;

        private bool _ready = false;
        private readonly string _rulesPath = "Rules\\Rules.clp";

        public MainForm()
        {
            InitializeComponent();

            dataGridView.AutoGenerateColumns = false;

            threeDComboBox.DataSource = _preferred3DChoices;
            resolutionComboBox.DataSource = _preferredResolutionChoices;
            curvedComboBox.DataSource = _preferredCurvedChoices;
            hdrComboBox.DataSource = _preferredHdrChoices;

            wifiComboBox.DataSource = _wiFiChoices;
            smartTvComboBox.DataSource = _smartTvChoices;
            hdmiComboBox.DataSource = _hdmiChoices;
            usbComboBox.DataSource = _usbChoices;
            screenSizeComboBox.DataSource = _sizeChoices;

            priceComboBox.DataSource = _priceChoices;

            _enviroment = new Mommosoft.ExpertSystem.Environment();
            _enviroment.Load(_rulesPath);
            _tvParser = new TvParser(_enviroment);

            Icon = Properties.Resources.TvIcon;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            RunExpertSystemQuery();

            _ready = true;
        }

        private void RunExpertSystemQuery()
        {
            _enviroment.Reset();

            _tvParser.ParseScreenSize((string)screenSizeComboBox.SelectedValue);
            _tvParser.Parse3D((string)threeDComboBox.SelectedValue);
            _tvParser.ParseResolution((string)resolutionComboBox.SelectedValue);
            _tvParser.ParseScreenCurved((string)curvedComboBox.SelectedValue);
            _tvParser.ParseHdr((string)hdrComboBox.SelectedValue);

            _tvParser.ParseWifi((string)wifiComboBox.SelectedValue);
            _tvParser.ParseSmartTv((string)smartTvComboBox.SelectedValue);
            _tvParser.ParseHdmi((string)hdmiComboBox.SelectedValue);
            _tvParser.ParseUsb((string)usbComboBox.SelectedValue);

            _tvParser.ParsePrice((string)priceComboBox.SelectedValue);
            _enviroment.Run();

            UpdateItemsDisplayed();
        }

        private void UpdateItemsDisplayed()
        {
            const string evalStr = "(TV::get-tv-list)";

            var mv = (MultifieldValue)_enviroment.Eval(evalStr);

            var itemsRecList = new List<ItemRecommendation>();

            foreach (var mvi in mv)
            {
                var fv = (FactAddressValue)mvi;

                var certainty = (int)((FloatValue)fv.GetFactSlot("certainty"));
                var wineName = ((StringValue)fv.GetFactSlot("value"));

                itemsRecList.Add(new ItemRecommendation() { ItemName = wineName, Certainty = certainty });
            }
            if (!itemsRecList.Any())
            {
                itemsRecList.Add(new ItemRecommendation() { Certainty =  0, ItemName = "Nie znaleziono żadnego odpowiedniego" });
            }

            dataGridView.DataSource = itemsRecList;
        }

        private void OnChange(object sender, EventArgs e)
        {
            if (_ready)
            {
                RunExpertSystemQuery();
            }
        }
    }
}
