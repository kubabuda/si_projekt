﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TelevisorChooser.Models
{
    public class ItemRecommendation
    {
        public string ItemName { get; set; }
        public int Certainty { get; set; }
    }
}
