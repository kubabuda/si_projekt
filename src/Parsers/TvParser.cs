﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TelevisorChooser.Parsers
{
    class TvParser
    {
        private readonly Mommosoft.ExpertSystem.Environment _enviroment;

        public TvParser(Mommosoft.ExpertSystem.Environment env)
            {
            _enviroment = env;
        }


        #region parsingGUIinput

        public void Parse3D(string item)
        {
            if (item.Equals("Tak"))
            {
                _enviroment.AssertString("(attribute (name preferred-3D) (value yes))");
            }
            else if (item.Equals("Nie"))
            {
                _enviroment.AssertString("(attribute (name preferred-3D) (value no))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name preferred-3D) (value unknown))");
            }
        }

        public void ParseResolution(string item)
        {
            if (item.Equals("HD"))
            {
                _enviroment.AssertString("(attribute (name preferred-resolution) (value HD))");
            }
            else if (item.Equals("Full HD"))
            {
                _enviroment.AssertString("(attribute (name preferred-resolution) (value FullHD))");
            }
            else if (item.Equals("Full"))
            {
                _enviroment.AssertString("(attribute (name preferred-resolution) (value 4K))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name preferred-resolution) (value unknown))");
            }
        }

        public void ParseScreenCurved(string item)
        {
            if (item.Equals("Tak"))
            {
                _enviroment.AssertString("(attribute (name preferred-screen-curved) (value yes))");
            }
            else if (item.Equals("Nie"))
            {
                _enviroment.AssertString("(attribute (name preferred-screen-curved) (value no))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name preferred-screen-curved) (value unknown))");
            }
        }

        public void ParseScreenSize(string item)
        {
            if (item.Equals("22 - 32"))
            {
                _enviroment.AssertString("(attribute (name preferred-screen-size) (value small))");
            }
            else if (item.Equals("33 - 40"))
            {
                _enviroment.AssertString("(attribute (name preferred-screen-size) (value medium))");
            }
            else if (item.Equals("41 - 65"))
            {
                _enviroment.AssertString("(attribute (name preferred-screen-size) (value big))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name preferred-screen-size) (value unknown))");
            }
        }

        public void ParseHdr(string item)
        {
            if (item.Equals("Tak"))
            {
                _enviroment.AssertString("(attribute (name preferred-HDR) (value yes))");
            }
            else if (item.Equals("Nie"))
            {
                _enviroment.AssertString("(attribute (name preferred-HDR) (value no))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name preferred-HDR) (value unknown))");
            }
        }

        public void ParseWifi(string item)
        {
            if (item.Equals("Tak"))
            {
                _enviroment.AssertString("(attribute (name preferred-wifi) (value yes))");
            }
            else if (item.Equals("Nie"))
            {
                _enviroment.AssertString("(attribute (name preferred-wifi) (value no))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name preferred-wifi) (value unknown))");
            }
        }

        public void ParseSmartTv(string item)
        {

            if (item.Equals("Tak"))
            {
                _enviroment.AssertString("(attribute (name preferred-smart) (value yes))");
            }
            else if (item.Equals("Nie"))
            {
                _enviroment.AssertString("(attribute (name preferred-smart) (value no))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name preferred-smart) (value unknown))");
            }
        }

        public void ParseHdmi(string item)
        {
            if (item.Equals("1"))
            {
                _enviroment.AssertString("(attribute (name min-hdmi) (value 1))");
            }
            else if (item.Equals("2"))
            {
                _enviroment.AssertString("(attribute (name min-hdmi) (value 2))");
            }
            else if (item.Equals("3"))
            {
                _enviroment.AssertString("(attribute (name min-hdmi) (value 3))");
            }
            else if (item.Equals("4"))
            {
                _enviroment.AssertString("(attribute (name min-hdmi) (value 4))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name min-hdmi) (value unknown))");
            }
        }

        public void ParseUsb(string item)
        {
            if (item.Equals("1"))
            {
                _enviroment.AssertString("(attribute (name min-usb) (value 1))");
            }
            else if (item.Equals("2"))
            {
                _enviroment.AssertString("(attribute (name min-usb) (value 2))");
            }
            else if (item.Equals("3"))
            {
                _enviroment.AssertString("(attribute (name min-usb) (value 3))");
            }
            else if (item.Equals("4"))
            {
                _enviroment.AssertString("(attribute (name min-usb) (value 4))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name min-usb) (value unknown))");
            }
        }

        public void ParsePrice(string item)
        {
            if (item.Equals("Mogę dopłacić"))
            {
                _enviroment.AssertString("(attribute (name user-attitude) (value premium))");
            }
            else if (item.Equals("Wolę oszczędzać"))
            {
                _enviroment.AssertString("(attribute (name user-attitude) (value budget))");
            }
            else
            {
                _enviroment.AssertString("(attribute (name user-attitude) (value unknown))");
            }
        }

        #endregion

    }
}
