﻿namespace TelevisorChooser {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.threeDComboBox = new System.Windows.Forms.ComboBox();
            this.imageGroupBox = new System.Windows.Forms.GroupBox();
            this.screenSizeComboBox = new System.Windows.Forms.ComboBox();
            this.screenSizeLabel = new System.Windows.Forms.Label();
            this.hdrLabel = new System.Windows.Forms.Label();
            this.hdrComboBox = new System.Windows.Forms.ComboBox();
            this.curvedLabel = new System.Windows.Forms.Label();
            this.resolutionLabel = new System.Windows.Forms.Label();
            this.threeDLabel = new System.Windows.Forms.Label();
            this.curvedComboBox = new System.Windows.Forms.ComboBox();
            this.resolutionComboBox = new System.Windows.Forms.ComboBox();
            this.connectivityGroupBox = new System.Windows.Forms.GroupBox();
            this.usbLabel = new System.Windows.Forms.Label();
            this.usbComboBox = new System.Windows.Forms.ComboBox();
            this.hdmiLabel = new System.Windows.Forms.Label();
            this.smartTvLabel = new System.Windows.Forms.Label();
            this.wifiLabel = new System.Windows.Forms.Label();
            this.hdmiComboBox = new System.Windows.Forms.ComboBox();
            this.smartTvComboBox = new System.Windows.Forms.ComboBox();
            this.wifiComboBox = new System.Windows.Forms.ComboBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.itemsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recWeightColumn = new Sample.DataGridViewProgressColumn();
            this.dataGridViewProgressColumn1 = new Sample.DataGridViewProgressColumn();
            this.priceGroupBox = new System.Windows.Forms.GroupBox();
            this.priceLabel = new System.Windows.Forms.Label();
            this.priceComboBox = new System.Windows.Forms.ComboBox();
            this.imageGroupBox.SuspendLayout();
            this.connectivityGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.priceGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // threeDComboBox
            // 
            this.threeDComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.threeDComboBox.FormattingEnabled = true;
            this.threeDComboBox.Location = new System.Drawing.Point(120, 75);
            this.threeDComboBox.Name = "threeDComboBox";
            this.threeDComboBox.Size = new System.Drawing.Size(121, 21);
            this.threeDComboBox.TabIndex = 0;
            this.threeDComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // imageGroupBox
            // 
            this.imageGroupBox.Controls.Add(this.screenSizeComboBox);
            this.imageGroupBox.Controls.Add(this.screenSizeLabel);
            this.imageGroupBox.Controls.Add(this.hdrLabel);
            this.imageGroupBox.Controls.Add(this.hdrComboBox);
            this.imageGroupBox.Controls.Add(this.curvedLabel);
            this.imageGroupBox.Controls.Add(this.resolutionLabel);
            this.imageGroupBox.Controls.Add(this.threeDLabel);
            this.imageGroupBox.Controls.Add(this.curvedComboBox);
            this.imageGroupBox.Controls.Add(this.resolutionComboBox);
            this.imageGroupBox.Controls.Add(this.threeDComboBox);
            this.imageGroupBox.Location = new System.Drawing.Point(12, 12);
            this.imageGroupBox.Name = "imageGroupBox";
            this.imageGroupBox.Size = new System.Drawing.Size(253, 190);
            this.imageGroupBox.TabIndex = 1;
            this.imageGroupBox.TabStop = false;
            this.imageGroupBox.Text = "Obraz";
            // 
            // screenSizeComboBox
            // 
            this.screenSizeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.screenSizeComboBox.FormattingEnabled = true;
            this.screenSizeComboBox.Location = new System.Drawing.Point(120, 21);
            this.screenSizeComboBox.Name = "screenSizeComboBox";
            this.screenSizeComboBox.Size = new System.Drawing.Size(121, 21);
            this.screenSizeComboBox.TabIndex = 9;
            this.screenSizeComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // screenSizeLabel
            // 
            this.screenSizeLabel.AutoSize = true;
            this.screenSizeLabel.Location = new System.Drawing.Point(6, 24);
            this.screenSizeLabel.Name = "screenSizeLabel";
            this.screenSizeLabel.Size = new System.Drawing.Size(95, 13);
            this.screenSizeLabel.TabIndex = 8;
            this.screenSizeLabel.Text = "Wielkość ekranu:";
            // 
            // hdrLabel
            // 
            this.hdrLabel.AutoSize = true;
            this.hdrLabel.Location = new System.Drawing.Point(6, 132);
            this.hdrLabel.Name = "hdrLabel";
            this.hdrLabel.Size = new System.Drawing.Size(33, 13);
            this.hdrLabel.TabIndex = 7;
            this.hdrLabel.Text = "HDR:";
            // 
            // hdrComboBox
            // 
            this.hdrComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hdrComboBox.FormattingEnabled = true;
            this.hdrComboBox.Location = new System.Drawing.Point(120, 129);
            this.hdrComboBox.Name = "hdrComboBox";
            this.hdrComboBox.Size = new System.Drawing.Size(121, 21);
            this.hdrComboBox.TabIndex = 6;
            this.hdrComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // curvedLabel
            // 
            this.curvedLabel.AutoSize = true;
            this.curvedLabel.Location = new System.Drawing.Point(6, 105);
            this.curvedLabel.Name = "curvedLabel";
            this.curvedLabel.Size = new System.Drawing.Size(73, 13);
            this.curvedLabel.TabIndex = 5;
            this.curvedLabel.Text = "Zakrzywiony:";
            // 
            // resolutionLabel
            // 
            this.resolutionLabel.AutoSize = true;
            this.resolutionLabel.Location = new System.Drawing.Point(6, 51);
            this.resolutionLabel.Name = "resolutionLabel";
            this.resolutionLabel.Size = new System.Drawing.Size(80, 13);
            this.resolutionLabel.TabIndex = 4;
            this.resolutionLabel.Text = "Rozdzielczość:";
            // 
            // threeDLabel
            // 
            this.threeDLabel.AutoSize = true;
            this.threeDLabel.Location = new System.Drawing.Point(6, 78);
            this.threeDLabel.Name = "threeDLabel";
            this.threeDLabel.Size = new System.Drawing.Size(24, 13);
            this.threeDLabel.TabIndex = 3;
            this.threeDLabel.Text = "3D:";
            // 
            // curvedComboBox
            // 
            this.curvedComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.curvedComboBox.FormattingEnabled = true;
            this.curvedComboBox.Location = new System.Drawing.Point(120, 102);
            this.curvedComboBox.Name = "curvedComboBox";
            this.curvedComboBox.Size = new System.Drawing.Size(121, 21);
            this.curvedComboBox.TabIndex = 2;
            this.curvedComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // resolutionComboBox
            // 
            this.resolutionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resolutionComboBox.FormattingEnabled = true;
            this.resolutionComboBox.Location = new System.Drawing.Point(120, 48);
            this.resolutionComboBox.Name = "resolutionComboBox";
            this.resolutionComboBox.Size = new System.Drawing.Size(121, 21);
            this.resolutionComboBox.TabIndex = 1;
            this.resolutionComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // connectivityGroupBox
            // 
            this.connectivityGroupBox.Controls.Add(this.usbLabel);
            this.connectivityGroupBox.Controls.Add(this.usbComboBox);
            this.connectivityGroupBox.Controls.Add(this.hdmiLabel);
            this.connectivityGroupBox.Controls.Add(this.smartTvLabel);
            this.connectivityGroupBox.Controls.Add(this.wifiLabel);
            this.connectivityGroupBox.Controls.Add(this.hdmiComboBox);
            this.connectivityGroupBox.Controls.Add(this.smartTvComboBox);
            this.connectivityGroupBox.Controls.Add(this.wifiComboBox);
            this.connectivityGroupBox.Location = new System.Drawing.Point(271, 12);
            this.connectivityGroupBox.Name = "connectivityGroupBox";
            this.connectivityGroupBox.Size = new System.Drawing.Size(253, 132);
            this.connectivityGroupBox.TabIndex = 2;
            this.connectivityGroupBox.TabStop = false;
            this.connectivityGroupBox.Text = "Łączność";
            // 
            // usbLabel
            // 
            this.usbLabel.AutoSize = true;
            this.usbLabel.Location = new System.Drawing.Point(8, 108);
            this.usbLabel.Name = "usbLabel";
            this.usbLabel.Size = new System.Drawing.Size(31, 13);
            this.usbLabel.TabIndex = 7;
            this.usbLabel.Text = "USB:";
            // 
            // usbComboBox
            // 
            this.usbComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.usbComboBox.FormattingEnabled = true;
            this.usbComboBox.Location = new System.Drawing.Point(122, 105);
            this.usbComboBox.Name = "usbComboBox";
            this.usbComboBox.Size = new System.Drawing.Size(121, 21);
            this.usbComboBox.TabIndex = 6;
            this.usbComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // hdmiLabel
            // 
            this.hdmiLabel.AutoSize = true;
            this.hdmiLabel.Location = new System.Drawing.Point(8, 78);
            this.hdmiLabel.Name = "hdmiLabel";
            this.hdmiLabel.Size = new System.Drawing.Size(39, 13);
            this.hdmiLabel.TabIndex = 5;
            this.hdmiLabel.Text = "HDMI:";
            // 
            // smartTvLabel
            // 
            this.smartTvLabel.AutoSize = true;
            this.smartTvLabel.Location = new System.Drawing.Point(6, 51);
            this.smartTvLabel.Name = "smartTvLabel";
            this.smartTvLabel.Size = new System.Drawing.Size(54, 13);
            this.smartTvLabel.TabIndex = 4;
            this.smartTvLabel.Text = "Smart TV:";
            // 
            // wifiLabel
            // 
            this.wifiLabel.AutoSize = true;
            this.wifiLabel.Location = new System.Drawing.Point(6, 24);
            this.wifiLabel.Name = "wifiLabel";
            this.wifiLabel.Size = new System.Drawing.Size(33, 13);
            this.wifiLabel.TabIndex = 3;
            this.wifiLabel.Text = "WiFi:";
            // 
            // hdmiComboBox
            // 
            this.hdmiComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hdmiComboBox.FormattingEnabled = true;
            this.hdmiComboBox.Location = new System.Drawing.Point(122, 75);
            this.hdmiComboBox.Name = "hdmiComboBox";
            this.hdmiComboBox.Size = new System.Drawing.Size(121, 21);
            this.hdmiComboBox.TabIndex = 2;
            this.hdmiComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // smartTvComboBox
            // 
            this.smartTvComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.smartTvComboBox.FormattingEnabled = true;
            this.smartTvComboBox.Location = new System.Drawing.Point(122, 48);
            this.smartTvComboBox.Name = "smartTvComboBox";
            this.smartTvComboBox.Size = new System.Drawing.Size(121, 21);
            this.smartTvComboBox.TabIndex = 1;
            this.smartTvComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // wifiComboBox
            // 
            this.wifiComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.wifiComboBox.FormattingEnabled = true;
            this.wifiComboBox.Location = new System.Drawing.Point(122, 21);
            this.wifiComboBox.Name = "wifiComboBox";
            this.wifiComboBox.Size = new System.Drawing.Size(121, 21);
            this.wifiComboBox.TabIndex = 0;
            this.wifiComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // dataGridView
            // 
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemsColumn,
            this.recWeightColumn});
            this.dataGridView.Location = new System.Drawing.Point(10, 208);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.Size = new System.Drawing.Size(512, 342);
            this.dataGridView.TabIndex = 3;
            // 
            // itemsColumn
            // 
            this.itemsColumn.DataPropertyName = "ItemName";
            this.itemsColumn.HeaderText = "Item";
            this.itemsColumn.Name = "itemsColumn";
            this.itemsColumn.ReadOnly = true;
            // 
            // recWeightColumn
            // 
            this.recWeightColumn.DataPropertyName = "Certainty";
            this.recWeightColumn.HeaderText = "Recommendation Weight";
            this.recWeightColumn.Name = "recWeightColumn";
            this.recWeightColumn.ReadOnly = true;
            this.recWeightColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.recWeightColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewProgressColumn1
            // 
            this.dataGridViewProgressColumn1.HeaderText = "Recommendation Weight";
            this.dataGridViewProgressColumn1.Name = "dataGridViewProgressColumn1";
            this.dataGridViewProgressColumn1.ReadOnly = true;
            this.dataGridViewProgressColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewProgressColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewProgressColumn1.Width = 254;
            // 
            // priceGroupBox
            // 
            this.priceGroupBox.Controls.Add(this.priceLabel);
            this.priceGroupBox.Controls.Add(this.priceComboBox);
            this.priceGroupBox.Location = new System.Drawing.Point(271, 150);
            this.priceGroupBox.Name = "priceGroupBox";
            this.priceGroupBox.Size = new System.Drawing.Size(253, 52);
            this.priceGroupBox.TabIndex = 8;
            this.priceGroupBox.TabStop = false;
            this.priceGroupBox.Text = "Cena";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(6, 24);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(86, 13);
            this.priceLabel.TabIndex = 3;
            this.priceLabel.Text = "W kwestii ceny:";
            // 
            // priceComboBox
            // 
            this.priceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.priceComboBox.FormattingEnabled = true;
            this.priceComboBox.Location = new System.Drawing.Point(122, 21);
            this.priceComboBox.Name = "priceComboBox";
            this.priceComboBox.Size = new System.Drawing.Size(121, 21);
            this.priceComboBox.TabIndex = 0;
            this.priceComboBox.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 562);
            this.Controls.Add(this.priceGroupBox);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.connectivityGroupBox);
            this.Controls.Add(this.imageGroupBox);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "TelevisorChooser";
            this.imageGroupBox.ResumeLayout(false);
            this.imageGroupBox.PerformLayout();
            this.connectivityGroupBox.ResumeLayout(false);
            this.connectivityGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.priceGroupBox.ResumeLayout(false);
            this.priceGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox threeDComboBox;
        private System.Windows.Forms.GroupBox imageGroupBox;
        private System.Windows.Forms.Label curvedLabel;
        private System.Windows.Forms.Label resolutionLabel;
        private System.Windows.Forms.Label threeDLabel;
        private System.Windows.Forms.ComboBox curvedComboBox;
        private System.Windows.Forms.ComboBox resolutionComboBox;
        private System.Windows.Forms.GroupBox connectivityGroupBox;
        private System.Windows.Forms.Label hdmiLabel;
        private System.Windows.Forms.Label smartTvLabel;
        private System.Windows.Forms.Label wifiLabel;
        private System.Windows.Forms.ComboBox hdmiComboBox;
        private System.Windows.Forms.ComboBox smartTvComboBox;
        private System.Windows.Forms.ComboBox wifiComboBox;
        private System.Windows.Forms.DataGridView dataGridView;
        private Sample.DataGridViewProgressColumn dataGridViewProgressColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemsColumn;
        private Sample.DataGridViewProgressColumn recWeightColumn;
        private System.Windows.Forms.Label hdrLabel;
        private System.Windows.Forms.ComboBox hdrComboBox;
        private System.Windows.Forms.Label usbLabel;
        private System.Windows.Forms.ComboBox usbComboBox;
        private System.Windows.Forms.Label screenSizeLabel;
        private System.Windows.Forms.ComboBox screenSizeComboBox;
        private System.Windows.Forms.GroupBox priceGroupBox;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.ComboBox priceComboBox;
    }
}

