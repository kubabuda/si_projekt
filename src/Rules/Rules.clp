;;;======================================================
;;;
;;;     CLIPS Version 6.3
;;;
;;;     For use with the CLIPS.NET
;;;======================================================

(defmodule MAIN (export ?ALL))

;;*****************
;;* INITIAL STATE *
;;*****************

(deftemplate MAIN::attribute
   (slot name)
   (slot value)
   (slot certainty (default 100.0)))

(defrule MAIN::start
  (declare (salience 10000))
  =>
  (set-fact-duplication TRUE)
  (focus CHOOSE-QUALITIES TV))

(defrule MAIN::combine-certainties ""
  (declare (salience 100)
           (auto-focus TRUE))
  ?rem1 <- (attribute (name ?rel) (value ?val) (certainty ?per1))
  ?rem2 <- (attribute (name ?rel) (value ?val) (certainty ?per2))
  (test (neq ?rem1 ?rem2))
  =>
  (retract ?rem1)
  (modify ?rem2 (certainty (/ (- (* 100 (+ ?per1 ?per2)) (* ?per1 ?per2)) 100))))


;;******************
;; The RULES module
;;******************

(defmodule RULES (import MAIN ?ALL) (export ?ALL))

(deftemplate RULES::rule
  (slot certainty (default 100.0))
  (multislot if)
  (multislot then))

(defrule RULES::throw-away-ands-in-antecedent
  ?f <- (rule (if and $?rest))
  =>
  (modify ?f (if ?rest)))

(defrule RULES::throw-away-ands-in-consequent
  ?f <- (rule (then and $?rest))
  =>
  (modify ?f (then ?rest)))

(defrule RULES::remove-is-condition-when-satisfied
  ?f <- (rule (certainty ?c1)
              (if ?attribute is ?value $?rest))
  (attribute (name ?attribute)
             (value ?value)
             (certainty ?c2))
  =>
  (modify ?f (certainty (min ?c1 ?c2)) (if ?rest)))

(defrule RULES::remove-is-not-condition-when-satisfied
  ?f <- (rule (certainty ?c1)
              (if ?attribute is-not ?value $?rest))
  (attribute (name ?attribute) (value ~?value) (certainty ?c2))
  =>
  (modify ?f (certainty (min ?c1 ?c2)) (if ?rest)))

(defrule RULES::perform-rule-consequent-with-certainty
  ?f <- (rule (certainty ?c1)
              (if)
              (then ?attribute is ?value with certainty ?c2 $?rest))
  =>
  (modify ?f (then ?rest))
  (assert (attribute (name ?attribute)
                     (value ?value)
                     (certainty (/ (* ?c1 ?c2) 100)))))

(defrule RULES::perform-rule-consequent-without-certainty
  ?f <- (rule (certainty ?c1)
              (if)
              (then ?attribute is ?value $?rest))
  (test (or (eq (length$ ?rest) 0)
            (neq (nth 1 ?rest) with)))
  =>
  (modify ?f (then ?rest))
  (assert (attribute (name ?attribute) (value ?value) (certainty ?c1))))

;;*******************************
;;* CHOOSE TV QUALITIES RULES *
;;*******************************

(defmodule CHOOSE-QUALITIES (import RULES ?ALL)
                            (import MAIN ?ALL))

(defrule CHOOSE-QUALITIES::startit => (focus RULES))

(deffacts the-tv-rules

  ; Rules for picking the best resolution

  (rule (if preferred-resolution is unknown)
        (then best-resolution is 4K with certainty 100 and
              best-resolution is FullHD with certainty 90 and
              best-resolution is HD with certainty 80))

  (rule (if preferred-resolution is 4K)
        (then best-resolution is 4K with certainty 100))

  (rule (if preferred-resolution is FullHD)
        (then best-resolution is FullHD with certainty 100 and
              best-resolution is 4K with certainty 70))

  (rule (if preferred-resolution is HD)
        (then best-resolution is HD with certainty 100 and
              best-resolution is FullHD with certainty 60 and
              best-resolution is 4K with certainty 50))

  (rule (if preferred-resolution is HD and user-attitude is premium)
        (then best-resolution is HD with certainty 100 and
              best-resolution is FullHD with certainty 90 and
              best-resolution is 4K with certainty 80))

  ; Rules for screen size

  (rule (if preferred-screen-size is unknown)
        (then best-screen-size is small with certainty 100 and
              best-screen-size is medium with certainty 100 and
              best-screen-size is big with certainty 100))

  (rule (if preferred-screen-size is small)
        (then best-screen-size is small with certainty 100 and
              best-screen-size is medium with certainty 60))


  (rule (if preferred-screen-size is medium)
        (then best-screen-size is small with certainty 70 and
              best-screen-size is medium with certainty 100 and
              best-screen-size is medium with certainty 70))

  (rule (if preferred-screen-size is big)
        (then best-screen-size is big with certainty 100 and
              best-screen-size is medium with certainty 60))

  ; Rules for picking 3D technology property

  (rule (if smart-tv is yes)
        (then best-3D is yes with certainty 90))

  (rule (if smart-tv is no)
        (then best-3D is no with certainty 90 and
              best-3D is yes with certainty 30))

  (rule (if smart-tv is unknown)
        (then best-3D is no with certainty 60 and
              best-3D is yes with certainty 80))

  (rule (if preferred-resolution is HD and best-3D is unknown)
        (then best-3D is no))

  (rule (if preferred-3D is yes)
        (then best-3D is yes with certainty 90))

  (rule (if preferred-3D is no)
        (then best-3D is no with certainty 100))

  (rule (if preferred-3D is unknown)
        (then best-3D is yes with certainty 80 and
              best-3D is no with certainty 70))


  ; Rules for picking screen curvature feature

  (rule (if preferred-screen-curved is yes)
        (then best-screen-curved is yes with certainty 100))

  (rule (if preferred-screen-curved is no)
        (then best-screen-curved is no with certainty 100))

  (rule (if preferred-screen-curved is unknown)
        (then best-screen-curved is yes with certainty 80 and
              best-screen-curved is no with certainty 70))
  ; Rules for HDR

  (rule (if preferred-HDR is yes)
        (then best-HDR is yes with certainty 100))

  (rule (if preferred-HDR is no)
        (then best-HDR is no with certainty 90 and
              best-HDR is yes with certainty 30))

  (rule (if preferred-HDR is unknown)
        (then best-HDR is no with certainty 90 and
              best-HDR is yes with certainty 80))

  ; Rules for wifi

  (rule (if preferred-wifi is yes)
        (then best-wifi is yes with certainty 90))

  (rule (if preferred-wifi is no)
        (then best-wifi is no with certainty 90))

  (rule (if preferred-wifi is unknown)
        (then best-wifi is no with certainty 90 and
              best-wifi is yes with certainty 90))

  ; Rules for smart

  (rule (if preferred-smart is yes)
        (then best-smart is yes with certainty 90))

  (rule (if preferred-smart is no)
        (then best-smart is no with certainty 90 and
              best-smart is yes with certainty 10))

  (rule (if preferred-smart is unknown)
        (then best-smart is no with certainty 90 and
              best-smart is yes with certainty 90))

  (rule (if smart-tv is unknown and user-attitude is premium)
        (then best-smart is no with certainty 50 and
              best-smart is yes with certainty 100))


  ; Rules for HDMI

  (rule (if min-hdmi is 1)
        (then best-hdmi is 1 with certainty 90 and
        best-hdmi is 2 with certainty 70 and
        best-hdmi is 3 with certainty 60 and
        best-hdmi is 4 with certainty 40))

  (rule (if min-hdmi is 1 and user-attitude is premium)
        (then best-hdmi is 1 with certainty 100 and
              best-hdmi is 2 with certainty 100 and
              best-hdmi is 3 with certainty 100 and
              best-hdmi is 4 with certainty 100))

  (rule (if min-hdmi is 2)
        (then best-hdmi is 2 with certainty 100 and
              best-hdmi is 3 with certainty 60 and
              best-hdmi is 4 with certainty 50))

(rule (if min-hdmi is 2 and user-attitude is premium)
        (then best-hdmi is 2 with certainty 100 and
              best-hdmi is 3 with certainty 100 and
              best-hdmi is 4 with certainty 100))

  (rule (if min-hdmi is 3)
        (then best-hdmi is 3 with certainty 100 and
              best-hdmi is 4 with certainty 95))

  (rule (if min-hdmi is 3 and user-attitude is premium)
        (then best-hdmi is 3 with certainty 100 and
              best-hdmi is 4 with certainty 100))

  (rule (if min-hdmi is 4)
        (then best-hdmi is 4 with certainty 100 and
        best-hdmi is 3 with certainty 50))

  (rule (if min-hdmi is unknown)
      (then best-hdmi is 4 with certainty 90 and
        best-hdmi is 3 with certainty 93 and
        best-hdmi is 2 with certainty 96 and
        best-hdmi is 1 with certainty 100))

  (rule (if min-hdmi is unknown and user-attitude is premium)
      (then best-hdmi is 4 with certainty 100 and
        best-hdmi is 3 with certainty 100 and
        best-hdmi is 2 with certainty 100 and
        best-hdmi is 1 with certainty 100))

  ; Rules for usb

  (rule (if min-usb is 1)
        (then best-usb is 1 with certainty 80 and
        best-usb is 2 with certainty 75 and
        best-usb is 4 with certainty 70))

  (rule (if min-usb is 2)
        (then best-usb is 1 with certainty 30 and
        best-usb is 2 with certainty 100 and
        best-usb is 3 with certainty 80))

  (rule (if min-usb is 3)
        (then best-usb is 1 with certainty 30 and
        best-usb is 2 with certainty 40 and
        best-usb is 3 with certainty 80))

  (rule (if min-usb is unknown)
    (then best-usb is 3 with certainty 100 and
    best-usb is 2 with certainty 100 and
    best-usb is 1 with certainty 100))
)

;;************************
;;* TV SELECTION RULES *
;;************************

(defmodule TV (import MAIN ?ALL)
                 (export deffunction get-tv-list))

(deftemplate TV::tv_model
  (slot name (default ?NONE))
  (slot price)
  (slot diameter)
  (multislot resolution (default HD))
  (slot screen-curved (default no))
  (slot wifi (default no))
  (slot smart-tv (default no))
  (slot HDMI (default 1))
  (slot USB (default 1))
  (slot 3D (default no))
  (slot HDR (default any))
  )

(deffacts TV::the-tv_models-list
  (attribute (name best-3D) (value unknown))
  (attribute (name best-resolution) (value unknown))
  (attribute (name best-screen-curved) (value unknown))

  (tv_model
    (name "LG 49UF7787")
    (price 2890 )
    (diameter 49)
    (resolution 4K)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 3)
    (USB 3)
    (3D no)
    (HDR no))

  (tv_model
    (name "Hitachi 24HBC05")
    (price 649)
    (diameter 24)
    (resolution HD)
    (screen-curved no)
    (wifi no)
    (smart-tv no)
    (HDMI 1)
    (USB 1)
    (3D no)
    (HDR no))

  (tv_model
    (name "LG 55EG910V Curved  7")
    (price 890)
    (diameter 55)
    (resolution FullHD)
    (screen-curved yes)
    (wifi yes)
    (smart-tv yes)
    (HDMI 3)
    (USB 3)
    (3D yes)
    (HDR no))

  (tv_model
    (name "Panasonic TX-40DXU701")
    (price 2999)
    (diameter 40)
    (resolution 4K)
    (screen-curved no)
    (wifi no)
    (smart-tv no)
    (HDMI 3)
    (USB 3)
    (3D no)
    (HDR yes))

  (tv_model
    (name "Samsung UE40KU6172U")
    (price 2899)
    (diameter 40)
    (resolution 4K)
    (screen-curved yes)
    (wifi yes)
    (smart-tv yes)
    (HDMI 3)
    (USB 2)
    (3D no)
    (HDR yes))

  (tv_model
    (name "Sony KDL-32WD605")
    (price 1599)
    (diameter 32)
    (resolution HD)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 2)
    (USB 2)
    (3D no)
    (HDR no))

  (tv_model
    (name "LG 55UH600V")
    (price 3899)
    (diameter 55)
    (resolution 4K)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 2)
    (USB 1)
    (3D no)
    (HDR no))

  (tv_model
    (name "Thomson 28HA3203W")
    (price 799)
    (diameter 28)
    (resolution HD)
    (screen-curved no)
    (wifi no)
    (smart-tv no)
    (HDMI 2)
    (USB 1)
    (3D no)
    (HDR no))

  (tv_model
    (name "Philips 65PUS8700/12 Curved")
    (price 10990)
    (diameter 65)
    (resolution 4K)
    (screen-curved yes)
    (wifi yes)
    (smart-tv yes)
    (HDMI 4)
    (USB 3)
    (3D yes)
    (HDR no))

  (tv_model
    (name "Samsung UE55KU6670S Curved")
    (price 4999)
    (diameter 55)
    (resolution 4K)
    (screen-curved yes)
    (wifi yes)
    (smart-tv yes)
    (HDMI 3)
    (USB 2)
    (3D no)
    (HDR yes))

  (tv_model
    (name "Panasonic TX-40C200E")
    (price 1299)
    (diameter 40)
    (resolution FullHD)
    (screen-curved no)
    (wifi no)
    (smart-tv no)
    (HDMI 1)
    (USB 1)
    (3D no)
    (HDR no))

  (tv_model
    (name "Panasonic TX-65CX410E")
    (price 6499)
    (diameter 65)
    (resolution 4K)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 4)
    (USB 3)
    (3D no)
    (HDR no))

  (tv_model
    (name "Samsung UE22H5610")
    (price 999)
    (diameter 22)
    (resolution FullHD)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 2)
    (USB 2)
    (3D no)
    (HDR no))

  (tv_model
    (name "LG 43UH6107")
    (price 2499)
    (diameter 43)
    (resolution 4K)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 3)
    (USB 1)
    (3D no)
    (HDR yes))

  (tv_model
    (name "Blaupunkt BLA-49/148O-GB-11B-FGBQKUP-EU")
    (price 1599)
    (diameter 49)
    (resolution FullHD)
    (screen-curved no)
    (wifi no)
    (smart-tv no)
    (HDMI 3)
    (USB 2)
    (3D no)
    (HDR no))

  (tv_model
    (name "Sony KDL-50W809C")
    (price 3599)
    (diameter 50)
    (resolution FullHD)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 4)
    (USB 3)
    (3D yes)
    (HDR no))

  (tv_model
    (name "Toshiba 84L9363DG")
    (price 39999)
    (diameter 84)
    (resolution 4K)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 4)
    (USB 2)
    (3D yes)
    (HDR no))

  (tv_model
    (name "Samsung UE55K6370AS Curved")
    (price 4499)
    (diameter 55)
    (resolution FullHD)
    (screen-curved yes)
    (wifi yes)
    (smart-tv yes)
    (HDMI 3)
    (USB 2)
    (3D no)
    (HDR no))

  (tv_model
    (name "LG 65UH600V")
    (price 5999)
    (diameter 65)
    (resolution 4K)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 2)
    (USB 1)
    (3D no)
    (HDR no))

  (tv_model
    (name "LG 55EG910V Curved")
    (price 7890)
    (diameter 55)
    (resolution FullHD)
    (screen-curved yes)
    (wifi yes)
    (smart-tv yes)
    (HDMI 3)
    (USB 3)
    (3D yes)
    (HDR no))

  (tv_model
    (name "LG 65UF950V")
    (price 9990)
    (diameter 65)
    (resolution 4K)
    (screen-curved no)
    (wifi yes)
    (smart-tv yes)
    (HDMI 3)
    (USB 3)
    (3D yes)
    (HDR no))
)

(defrule TV::is-tv-big
  (tv_model (name ?name)
            (diameter ?diameter&:( > ?diameter 40)))
  =>
  (assert (tv-size-range ?name big)))

(defrule TV::is-tv-medium
  (tv_model (name ?name)
            (diameter ?diameter&:(and (<= ?diameter 42)
                                      (>= ?diameter 31))))
  =>
  (assert (tv-size-range ?name medium)))

(defrule TV::is-tv-small
  (tv_model (name ?name)
            (diameter ?diameter&:(> 31 ?diameter)))
  =>
  (assert (tv-size-range ?name small)))


(defrule TV::is-size-ok
  (tv-size-range ?name ?size)
  (attribute (name preferred-screen-size) (value ?size))
  =>
  (assert (tv-size-range ?name allowed)))

(defrule TV::any-size-will-do
  (tv_model (name ?name))
  (attribute (name preferred-screen-size) (value unknown))
  =>
  (assert (tv-size-range ?name allowed)))


(defrule TV::generate-tv_model-attributes
  (tv_model (name ?name)
            (3D ?c)
            (resolution $? ?r $?)
            (screen-curved ?s)
            (HDR ?h)
            (wifi ?w)
            (smart-tv ?sm)
            (HDMI ?hdmi)
            (USB ?usb))
  (attribute (name best-3D) (value ?c) (certainty ?certainty-1))
  (attribute (name best-resolution) (value ?r) (certainty ?certainty-2))
  (attribute (name best-screen-curved) (value ?s) (certainty ?certainty-3))
  (attribute (name best-HDR) (value ?h) (certainty ?certainty-4))
  (attribute (name best-wifi) (value ?w) (certainty ?certainty-5))
  (attribute (name best-smart) (value ?sm) (certainty ?certainty-6))
  (attribute (name best-hdmi) (value ?hdmi) (certainty ?certainty-7))
  (attribute (name best-usb) (value ?usb) (certainty ?certainty-8))
  (tv-size-range ?name allowed)
  =>
  (assert (attribute (name tv_model ) (value ?name)
                     (certainty (min ?certainty-1 ?certainty-2 ?certainty-3 ?certainty-4 ?certainty-5 ?certainty-6 ?certainty-7 ?certainty-8 )))))

(deffunction TV::sort-by-certainty (?w1 ?w2)
   (< (fact-slot-value ?w1 certainty)
      (fact-slot-value ?w2 certainty)))

(deffunction TV::get-tv-list ()
  (bind ?facts (find-all-facts ((?f attribute))
                               (and (eq ?f:name tv_model )
                                    (>= ?f:certainty 20))))
  (sort sort-by-certainty ?facts))

;; TV Icon courtesy David Cross http://webhostingmedia.net/
